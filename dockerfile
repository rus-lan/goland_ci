# docker build -t goland_ci .
FROM golang:1.14.2
COPY . .
RUN cd src && go test -v -coverprofile .testCoverage.txt
