###Download script
```bash
wget -O /tmp/release.zip 'https://gitlab.com/rus-lan/goland_ci/-/jobs/artifacts/v1.0.0/download?job=release' \
&& unzip /tmp/release.zip -d ~/
```

###Run script
```bash
~/release-v1.0.0 .
```