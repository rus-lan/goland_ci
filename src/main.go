package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
)

type replaceType string

// replace method to regexp pack
func (s replaceType) Replace(exp string, repl string) replaceType {
	return replaceType(regexp.MustCompile(exp).ReplaceAllString(string(s), repl))
}

// match method to regexp pack
func (s replaceType) Match(exp string) bool {
	return regexp.MustCompile(exp).FindString(string(s)) != ""
}

// match method to regexp pack
func getFileSize(filepath string) (int64, error) {
	fi, err := os.Stat(filepath)
	if err != nil {
		return 0, err
	}
	// get the size
	return fi.Size(), nil
}

func getFileName(before string, end bool, path string) (string, string) {
	var link string
	var beforeList string

	if end {
		link = "└"
		beforeList = before + "\t"
	} else {
		link = "├"
		beforeList = before + "│\t"
	}

	fullName := replaceType(path).
		Replace(`^./`, "").
		Replace(`([\w.-]+)/`, "│\t").
		Replace(`(.*)\t([\S]+)$`, link+"───$2")

	return before + string(fullName), beforeList
}

func getCountDirs(files []os.FileInfo) int {
	var count int

	for _, info := range files {
		if info.IsDir() {
			count++
		}
	}

	return count
}

func dirReadToDir(out io.Writer, path string, printFiles bool, before string, files []os.FileInfo) error {
	countDir := getCountDirs(files)
	var count int

	for _, info := range files {
		if info.IsDir() {
			count++
			fullName, beforeList := getFileName(before, countDir == count, path+"/"+info.Name())

			fmt.Fprintln(out, fullName)
			err := dirRead(out, path+"/"+info.Name(), printFiles, beforeList)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func dirReadToFile(out io.Writer, path string, printFiles bool, before string, files []os.FileInfo) error {
	var count int

	for _, info := range files {
		if info.IsDir() {
			count++
			fullName, beforeList := getFileName(before, len(files) == count, path+"/"+info.Name())

			fmt.Fprintln(out, fullName)
			if err := dirRead(out, path+"/"+info.Name(), printFiles, beforeList); err != nil {
				return err
			}
		} else {
			count++
			fullName, _ := getFileName(before, len(files) == count, path+"/"+info.Name())

			size, err := getFileSize(path + "/" + info.Name())
			if err != nil {
				return err
			}

			sizeText := "empty"
			if size != 0 {
				sizeText = strconv.FormatInt(size, 10) + "b"
			}

			fmt.Fprintln(out, fullName+" ("+sizeText+")")
		}
	}

	return nil
}

func dirRead(out io.Writer, path string, printFiles bool, before string) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	if printFiles {
		if err := dirReadToFile(out, path, printFiles, before, files); err != nil {
			return err
		}
	} else {
		if err := dirReadToDir(out, path, printFiles, before, files); err != nil {
			return err
		}
	}

	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	if path == "testdata" {
		path = "../testdata"
	}

	if err := dirRead(out, path, printFiles, ""); err != nil {
		return err
	}

	return nil
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"

	if err := dirTree(out, path, printFiles); err != nil {
		panic(err.Error())
	}
}
